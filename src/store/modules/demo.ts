import axios from "axios";
import Vue from "vue";
import Vuex from "vuex";

export interface IMovieInformation {
  movie_count: number;
  limit: number;
  page_number: number;
}
export const MovieInformation: IMovieInformation = {
  movie_count: 0,
  limit: 0,
  page_number: 0,
};

export default {
  namespaced: true,
  state: {
    MovieInformation: [],
  },
  mutations: {
    setMovieData(state, payload: IMovieInformation = MovieInformation) {
      state.MovieInformation = payload;

      //   console.log(payload);
    },
  },
  actions: {
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async getMovieData({ commit }) {
      try {
        const res = await axios.get("https://yts.mx/api/v2/list_movies.json");
        // console.log(res.data.data);
        commit("setMovieData", res.data);
      } catch (error) {
        console.log(error);
      }
    },
  },
};
